//
// Created by amer0 on 11/2/2017.
//

#ifndef LAB_EXAM2_NODE_H
#define LAB_EXAM2_NODE_H

#include<string>

class node {
public:
    node *next, *prev;
    int data;
    std::string key;
    explicit node( std::string key,int data) : key(key), data(data), next(nullptr), prev(nullptr) {}
};

#endif //LAB_EXAM2_NODE_H
