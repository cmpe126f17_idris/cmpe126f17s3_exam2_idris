#include "map.h"
#include <string>
#include <iostream>
#include "node.h"

map::map()
{
    // Initialize linked list (map) values are initialized
    this->size = 0;
    this->head = nullptr;
    this->tail = nullptr;
}

map::~map()
{
    //created two temp pointers
    node *previous, *current;
    //current points to head
    current  =  head;

    //traverse the list
    while(current!= nullptr)
    {
        //previous points current
        previous = current;
        //current moves forward by one
        current = current->next;
        //delete the previous node
        delete previous;
    }

    this->head = this->tail = nullptr;
    this->size = 0;
    delete this;
}

void map::insert(std::string key, int value) {


        //case 1: list is empty
        if (head == nullptr && tail == nullptr) {
            node *temp = new node(key, value);
            head = tail = temp;
            this->size++;

            return;
        }

      //case 2: key already exists do nothing
        else if(find_key(key))
        {
           std::cout<<"Key already exists"<<std::endl;
            return;
        }

        //case 3: Key doesnt exist and list is not empty... adding at the end of the list
        else{
            node* temp1;

            temp1 = new node(key, value);

            tail->next = temp1;
            tail = tail->next;
            this->size++;
            return;
        }
}
void map::replace(std::string key, int value)
{
    //case 1: list is empty, so nothing to replace
    if(head == nullptr && tail == nullptr)
    {
        std::cout<<"list is empty!"<<std::endl;
        return;
    }
    //key doesnt exist in the list.. throw an exception
    if(!find_key(key))
    {
        std::cout<<"Key is not found in the list. Error!"<<std::endl;
        exit(1);
    }
    //Key if in the list
    else{
        node* temp;
        temp = head;
        while(temp != nullptr)
        {
            if(temp -> key == key)
            {
                temp->data = value;
            }

            temp = temp->next;
        }
    }



}

int map::get(std::string key) {

    //case 1: list is empty
    if (head == nullptr && tail == nullptr) {
        std::cerr << "error! empty list" << std::endl;
        exit(0);
    }

        //case 2: key doesn't exists

    else if (!find_key(key)) {

        std::cerr << "Error! Key is not in the list" << std::endl;
        exit(0);
    }


        //Key exists
    else {

        // create temp pointer
        node *temp;
        //temp points to the beginning of the list
        temp = head;

        //
        while (temp != nullptr) {
            if (temp->key == key) {
                return temp->data;
            }

            temp = temp->next;
        }

        return 0;
    }
}

bool map::erase(std::string key)
{
    //case 1: list is empty
    if (head == nullptr && tail == nullptr) {
        std::cerr << "Error! Empty list" << std::endl;
        exit(0);
    }

        //case 2: key doesn't exists

    else if (!find_key(key)) {

        std::cerr << "Error! Key is not in the list" << std::endl;
        exit(0);
    }

    else
    {
        node *temp1, *temp2, *temp3;
        temp1 = head;

        //case 1 removing a key at the beginning
        if(head->prev == nullptr && head->next != nullptr)
        {
            temp1 = head;
            temp2 = head->next;
            head = temp2;
            head->prev = nullptr;
            delete temp1;
            this->size--;
            return true;

        }
        //case 2: key is in the middle
        else if(head->next != nullptr && tail->prev != nullptr)
        {
            temp1 = head;
            while(temp1 != nullptr)
            {
                if(temp1->key == key)
                {
                    temp2= temp1->prev;
                    temp3= temp1->next;
                    temp2->next = temp3;
                    temp3->prev = temp2;
                    delete temp1;
                    this->size--;

                }
                temp1 = temp1->next;
            }

            return true;
        }


        //Key is at the end!
        else{
            temp1= tail;
            tail = tail->prev;
            delete temp1;
            this->size--;
            return true;

        }
    }

}

void map::print()
{
    //case 1: list is empty
    if (head == nullptr && tail == nullptr) {
        std::cerr << "Error! Empty list" << std::endl;
        exit(0);
    }


    //case 2: List is not empty
    else {
        node *temp;
        temp = head;
        while(temp != nullptr)
        {
            std::cout<<temp->key<<" with a value of: "<<temp->data<<std::endl;
            temp = temp->next;
        }

    }

    return;
}


//similar to the destructor
void map::clear()
{
//created two temp pointers
    node *previous, *current;
    //current points to head
    current  =  head;
    //previous is intialized
    previous = nullptr;

    //traverse the list
    while(current!= nullptr)
    {
        //previous points current
        previous = current;
        //current moves forward by one
        current = current->next;
        //delete the previous node
        delete previous;
    }

    this->head = this->tail = nullptr;
    this->size = 0;
}

int& map::operator[](std::string key)
{
    //case 1: list is empty
    if (head == nullptr && tail == nullptr) {
        std::cerr << "Error! Empty list" << std::endl;
        exit(0);
    }
    //key exists return reference to the location of Key
    else if(find_key(key) && head != nullptr && tail != nullptr)
    {
        //create a node
        node *temp;
        //assign temp to head
        temp = head;
        //traverse through the list
        while(temp != nullptr)
        {
            //if key found return a reference to key value
            if(temp->key == key)
            {
                return temp->data;
            }
            temp = temp->next;
        }
    }

}

bool map::find_key(const std::string key) {
        //create a node find
        node* find;
        //assign find to head
        find = head;

        //traverse the list checking for the value of key
        while(find != nullptr)
        {
            if(find->key == key)
            {
                return true;
            }

            find = find->next;

        }

        return false;

    }
